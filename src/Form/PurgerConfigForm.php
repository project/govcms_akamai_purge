<?php

namespace Drupal\govcms_akamai_purge\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\purge_ui\Form\PurgerConfigFormBase;

/**
 * Configuration form for the GovCMS Akamai Purge purger.
 */
class PurgerConfigForm extends PurgerConfigFormBase {

  const SETTINGS = 'govcms_akamai_purge.settings';

  /**
   * {@inheritdoc}
   */
  function getFormId() {
    return "govcms_akamai_purge_settings";
  }

  /**
   * {@inheritdoc}
   */
  function getEditableConfigNames() {
    return [
        static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::SETTINGS);
    $form['tabs'] = ['#type' => 'vertical_tabs', '#weight' => 10];
    $form['exclusion'] = [
      '#type' => 'details',
      '#group' => 'tabs',
      '#title' => $this->t('Exclusions'),
      '#description' => $this->t('Remove tags matching these patterns from the ban list'),
    ];
    $form['exclusion']['tag_exclusions'] = [
      '#title' => $this->t('Tag Exclusions'),
      '#type' => 'textarea',
      '#default_value' => implode(PHP_EOL, $config->get('tag_exclusions')),
    ];
    $form['blocklist'] = [
      '#type' => 'details',
      '#group' => 'tabs',
      '#title' => $this->t('Blocklist'),
      '#description' => $this->t('Conifgure tags to prevent sending to purge service, if a user event contains these tags the request will be skipped and not sent to the purge service.'),
    ];
    $form['blocklist']['tag_blocklist'] = [
      '#title' => $this->t('Tag Blocklist'),
      '#type' => 'textarea',
      '#default_value' => implode(PHP_EOL, $config->get('tag_blocklist')),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormSuccess(array &$form, FormStateInterface $form_state) {
    $tag_blocklist = explode(PHP_EOL, $form_state->getValue('tag_blocklist'));
    $tag_exclusions = explode(PHP_EOL, $form_state->getValue('tag_exclusions'));

    foreach ($tag_blocklist as &$tag) {
      $tag = preg_replace('~[[:cntrl:]]~', '', $tag);
    }
    foreach ($tag_exclusions as &$tag) {
      $tag = preg_replace('~[[:cntrl:]]~', '', $tag);
    }

    $this->config(self::SETTINGS)
      ->set('tag_blocklist', $tag_blocklist)
      ->set('tag_exclusions', $tag_exclusions)
      ->save();
  }

}