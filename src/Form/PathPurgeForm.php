<?php

namespace Drupal\govcms_akamai_purge\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\govcms_akamai_purge\PathPurger;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Exception\ClientException;

/**
 * Contains the path purge form.
 */
class PathPurgeForm extends ConfigFormBase {

  /**
   * Settings name.
   *
   * @var string
   */
  public const SETTINGS = 'govcms_akamai_purge.paths';

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\govcms_akamai_purge\PathPurger $pathPurger
   *   Path Purger service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    protected PathPurger $pathPurger,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) : self {
    return new self(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('govcms_akamai_purge.path_purger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'govcms_akamai_purge_paths_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() : array {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) : array {
    $error = !$this->pathPurger->checkEnvironmentSetup();
    if ($error) {
      $this->messenger()->addError($this->t('GovCMS Akamai purge is not configured correctly, please contact support to restore functionality.'));
    }

    $form['path_list'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Path list'),
      '#description' => $this->t('Please provide a list of URLs that will be invalidated from Akamai\'s edge cache. You need to provide URL paths separated by newlines in the textarea above, the URL paths should not contain the domain'),
      '#disabled' => $error,
      '#required' => TRUE,
    ];


    $form = parent::buildForm($form, $form_state);
    $form['actions']['submit']['#value'] = $this->t('Purge');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) : void {
    $paths = $form_state->getValue('path_list');
    $paths = explode(PHP_EOL, $paths);

    $invalid_paths = $this->pathPurger->validatePaths($paths);
    if ($invalid_paths === FALSE) {
      $form_state->setErrorByName('path_list', $this->t('Please provide valid paths.'));
      return;
    }

    if (is_array($invalid_paths) && count($invalid_paths) > 0) {
      $form_state->setErrorByName('path_list', $this->t('Please review and update as expected.'));
      $this->messenger()->addError($this->formatPlural(
        count($invalid_paths),
        'The path that you have submitted is not in the correct format',
        'The paths that you have submitted are not in the correct format'
      ));
      foreach ($invalid_paths as $path) {
        $this->messenger()->addError(" - $path");
      }

      return;
    }

    if (count($paths) > 200) {
      $form_state->setErrorByName('path_list', $this->t('Unable to process more than 200 paths in a single request.'));
      return;
    }

    $form_state->setValue('path_list', implode(PHP_EOL, $paths));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) : void {
    $paths = $form_state->getValue('path_list');
    $paths = explode(PHP_EOL, $paths);

    try {
      $this->pathPurger->purge($paths);
    }
    catch (ClientException | GuzzleException | \Exception $e) {
      $this->messenger()->addError('Unable to purge paths at this time, please contact support.');
      $form_state->setRebuild(TRUE);
      return;
    }

    $this->messenger()->addStatus('Your request has successfully been sent for processing. Please allow 10 minutes for the request to be actioned.');
  }

}
