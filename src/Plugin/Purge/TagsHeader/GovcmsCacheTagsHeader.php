<?php

namespace Drupal\govcms_akamai_purge\Plugin\Purge\TagsHeader;

use Drupal\govcms_akamai_purge\Hash;
use Drupal\purge\Plugin\Purge\TagsHeader\TagsHeaderBase;
use Drupal\purge\Plugin\Purge\TagsHeader\TagsHeaderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sets and formats the default response header with cache tags.
 *
 * @PurgeTagsHeader(
 *   id = "govcms_tagsheader",
 *   header_name = "Edge-Cache-Tag",
 * )
 */
class GovcmsCacheTagsHeader extends TagsHeaderBase implements TagsHeaderInterface {

  /**
   * Whether to send cacheability headers for debugging purposes.
   *
   * @var bool
   */
  protected $debugCacheabilityHeaders = FALSE;

  /**
   * The default maximum number of tags that can be pushed in headers
   * can be overridden by the env var GOVCMS_CACHE_TAG_MAX_COUNT
   */
  const DEFAULT_CACHE_TAG_MAX_COUNT = 0;

  /**
   * The default length of concatenated tags that can be pushed in headers
   * can be overridden by the env var GOVCMS_CACHE_TAG_MAX_LENGTH
   */
  const DEFAULT_CACHE_TAG_MAX_LENGTH = 0;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $http_response_debug_cacheability_headers = FALSE) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->debugCacheabilityHeaders = $http_response_debug_cacheability_headers;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('http.response.debug_cacheability_headers')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(array $tags) {
    $project = getenv('LAGOON_PROJECT') . '-' . getenv('LAGOON_GIT_SAFE_BRANCH') . ':';
    // Hash tags when not debugging cacheability headers.
    if (!$this->debugCacheabilityHeaders) {
      $tags = Hash::cacheTags($tags, $project);
    }
    else {
      foreach ($tags as &$tag) {
        $tag = $project . $tag;
      }
    }

    $tags = $this->truncateTheCacheHeaders($tags);

    return implode(',', $tags);
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (getenv('AKAMAI_PURGE_TOKEN') && getenv('LAGOON_PROJECT') && getenv('LAGOON_GIT_SAFE_BRANCH'));
  }

  /**
   * Takes a tag array and truncates it either by number of tags, total length
   * of combined header length, or both.
   * Defaults are given as class constants DEFAULT_CACHE_TAG_MAX_COUNT and
   * DEFAULT_CACHE_TAG_MAX_LENGTH with env vars GOVCMS_CACHE_TAG_MAX_COUNT and
   * GOVCMS_CACHE_TAG_MAX_LENGTH giving environmental overrides.
   *
   * Function only responds to max count or length values if they're over 0
   *
   * @param array $tags
   *
   * @return array
   */
  protected function truncateTheCacheHeaders(array $tags): array {
    // Truncate the cache headers by count if necessary
    $maxNumberOfHeaderTags = getenv('GOVCMS_CACHE_TAG_MAX_COUNT') ?: self::DEFAULT_CACHE_TAG_MAX_COUNT;
    if (!$this->debugCacheabilityHeaders && $maxNumberOfHeaderTags > 0 && count($tags) > $maxNumberOfHeaderTags) {
      $tags = array_slice($tags, 0, $maxNumberOfHeaderTags);
    }

    // Truncate the cache headers by length if necessary
    $maxConcatHeaderLength = getenv('GOVCMS_CACHE_TAG_MAX_LENGTH') ?: self::DEFAULT_CACHE_TAG_MAX_LENGTH;
    if (!$this->debugCacheabilityHeaders && $maxConcatHeaderLength > 0) {
      while(strlen(implode(",", $tags)) > $maxConcatHeaderLength) {
        array_pop($tags);
      }
    }

    return $tags;
  }

}
