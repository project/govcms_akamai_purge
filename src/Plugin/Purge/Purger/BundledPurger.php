<?php

namespace Drupal\govcms_akamai_purge\Plugin\Purge\Purger;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface;
use Drupal\purge\Plugin\Purge\Purger\PurgerBase;
use Drupal\purge\Plugin\Purge\Purger\PurgerInterface;
use Drupal\purge\Plugin\Purge\Invalidation\TagInvalidation;
use Drupal\govcms_akamai_purge\Hash;
use Drupal\Core\Config\ImmutableConfig;
use GuzzleHttp\ClientInterface;


/**
 * GovCMS Bundled Purger.
 *
 * @PurgePurger(
 *   id = "govcms_httpbundled",
 *   label = @Translation("GovCMS Bundled Purger"),
 *   configform = "\Drupal\govcms_akamai_purge\Form\PurgerConfigForm",
 *   cooldown_time = 0.0,
 *   description = @Translation("Configurable purger that sends a request to the Akamai Purge Service for a set of invalidation instructions."),
 *   multi_instance = FALSE,
 *   types = {"tag", "everything"},
 * )
 */
class BundledPurger extends PurgerBase implements PurgerInterface {

  /**
   * The Guzzle HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The configuration for this project.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;


  /**
   * Whether to send cacheability headers for debugging purposes.
   *
   * @var bool
   */
  protected $debugCacheabilityHeaders;

  /**
   * Constructs the HTTP purger.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   An HTTP client that can perform remote requests.
   */
  final public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientInterface $http_client, ImmutableConfig $settings, $http_response_debug_cacheability_headers = FALSE) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $http_client;
    $this->debugCacheabilityHeaders = $http_response_debug_cacheability_headers;
    $this->settings = $settings;
  }

  /**
   * Returns only tag invalidation types in the provided array.
   * 
   * @param array $invalidations
   *   The invalidations array.
   * 
   * @return array
   */
  private function getTagInvalidations(array $invalidations) {
    $filtered_array = [];
    foreach($invalidations as $invalidation) {
      if ($invalidation->getType() == 'tag') {
        $filtered_array[] = $invalidation;
      }
    }
    return $filtered_array;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('config.factory')->get('govcms_akamai_purge.settings'),
      $container->getParameter('http.response.debug_cacheability_headers')
    );
  }

  /**
   * Get the purge service hostname.
   *
   * @return string
   *   The purge service URI.
   */
  public function getUri() {
    $scheme = getenv('AKAMAI_PURGE_SERVICE_SCHEME') ?: 'http';
    $host = getenv('AKAMAI_PURGE_SERVICE_HOSTNAME') ?: "akamai-purge-service";
    $port = getenv('AKAMAI_PURGE_SERVICE_PORT') ?: 80;
    $path = getenv('AKAMAI_PURGE_SERVICE_PATH') ?: '/purge';
    return sprintf(
      '%s://%s:%s%s',
      $scheme,
      $host,
      $port,
      $path
    );
  }

  /**
   * Get the configured purge token.
   *
   * @return string
   *   The request token.
   */
  public function getPurgeToken() {
    return getenv('AKAMAI_PURGE_TOKEN');
  }

  /**
   * Is the site set to dev mode
   *
   * @return bool
   *   Whether DEV_MODE=true
   */
  public function isDevMode() {
    return strtolower(trim(getenv('DEV_MODE'))) === "true";
  }

  /**
   * Get the project name.
   *
   * @return string
   *   The lagoon project.
   */
  public function getProject() {
    return getenv('LAGOON_PROJECT');
  }

  /**
   * Get the environment name
   *
   * @return string
   *   The environment name.
   */
  public function getEnvironment() {
    return getenv('LAGOON_GIT_SAFE_BRANCH');
  }

  /**
   * Get Guzzle request options.
   *
   * @param array $invalidations
   *   The token data for this invalidation event.
   *
   * @return array
   *   Guzzle options.
   */
  public function getOptions(array $invalidations) {
    $opt = [
      'http_errors' => TRUE,
      'connect_timeout' => 1,
      'timeout' => 5,
      'headers' => [
        'X-Purge-Token' => $this->getPurgeToken(),
        'X-Lagoon-Project' => $this->getProject(),
        'Content-Type' => 'application/json',
      ],
      'verify' => FALSE,
    ];

    $opt['json'] = [
      'bundled_tags' => $this->prepareTags($invalidations),
    ];

    return $opt;
  }

  /**
   * Prepare the invalidation expression.
   *
   * @param array $invalidations
   *   The token data objects.
   * @param string $separator
   *   The separator to join on.
   *
   * @return string
   *   A list of tag expressions to ban
   */
  public function prepareTags(array $invalidations = [], $separator = "|") {
    $tag_prefix = $this->getProject() . '-' . $this->getEnvironment() . ':';
    $tag_exclusions = is_array($this->settings->get('tag_exclusions')) ? $this->settings->get('tag_exclusions') : [];
    $expressions = [];

    /** @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface $invalidation */
    foreach ($invalidations as $invalidation) {
      $expression = $invalidation->getExpression();
      if (!$invalidation instanceof TagInvalidation || !$invalidation->getProperty('govcms') || !is_string($expression)) {
        continue;
      }

      if (in_array($expression, $tag_exclusions) && empty($invalidation->getProperty('force'))) {
        continue;
      }

      if (!$this->debugCacheabilityHeaders) {
        $expressions[] = Hash::cacheTag($expression, $tag_prefix);
      }
      else {
        $expressions[] = $tag_prefix . $expression;
      }
    }
    return implode($separator, $expressions);
  }

  public function hasRuntimeMeasurement() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function invalidate(array $invalidations) {
    $invalidate_everything = false;

    // Check to see if any incoming invalidations are
    // of the 'everything' type.
    foreach ($invalidations as $invalidation) {
      if ($invalidation->getType() == 'everything') {
        $invalidate_everything = true;
        break;
      }
    }

    $tag_invalidations = $this->getTagInvalidations($invalidations);

    if ($invalidate_everything) {
      $invalidation_factory = \Drupal::service('purge.invalidation.factory');
      $http_response_invalidation = $invalidation_factory->get('tag', 'http_response');
      $http_response_invalidation->setStateContext($this->getId());
      $http_response_invalidation->setProperty('force', TRUE);
      // Finally we append the 'http_response' tag invalidation.
      $tag_invalidations[] = $http_response_invalidation;
    }

    // Create a simple closure to mass-update states on the objects.
    $set_state = function ($state) use ($invalidations) {
      foreach ($invalidations as $invalidation) {
        $invalidation->setState($state);
      }
    };

    // If we're in dev mode, we can mark items as invalidated
    if ($this->isDevMode() === TRUE) {
      $this->logger()->info("Site in in DEV_MODE, skipping purge");
      $set_state(invalidationInterface::SUCCEEDED);
      return;
    }

    // Validate the project configuration.
    if (!$this->getPurgeToken() || !$this->getProject() || !$this->getEnvironment()) {
      $this->logger()->notice("Configuration missing for Akamai Purge");
      $set_state(InvalidationInterface::FAILED);
      return;
    }

    $expressions = [];
    // Inform tokens it's govcms.
    foreach ($tag_invalidations as $invalidation) {
      $invalidation->setProperty('govcms', TRUE);
      // Also collect all tag expressions.
      $exp = $invalidation->getExpression();
      array_push($expressions, $exp);
    }

    // Check for published tag.
    $tag_blocklist = is_array($this->settings->get('tag_blocklist')) ? $this->settings->get('tag_blocklist') : [];

    // Check if invalidations contain any blocklisted tags.
    if (count(array_intersect($expressions, $tag_blocklist)) !== 0) {
      // Simply set invalidation as success but do not send them.
      $set_state(InvalidationInterface::SUCCEEDED);
      return;
    }

    // Build up a single HTTP request, execute it and log errors.
    $uri = $this->getUri($tag_invalidations);
    $opt = $this->getOptions($tag_invalidations);

    try {
      $this->client->request("POST", $uri, $opt);
      $set_state(InvalidationInterface::SUCCEEDED);
    }
    catch (\Exception $e) {
      $set_state(InvalidationInterface::FAILED);
      // Log as much useful information as we can.
      $headers = $opt['headers'];
      unset($opt['headers']);
      $debug = json_encode([
        'msg' => $e->getMessage(),
        'uri' => $uri,
        'method' => $this->settings->request_method,
        'guzzle_opt' => $opt,
        'headers' => $headers,
      ], JSON_PRETTY_PRINT);
      $this->logger()->emergency("@e, details (JSON): @debug", [
        '@e' => json_encode($expressions),
        '@debug' => $debug
      ]);
    }
  }

}
