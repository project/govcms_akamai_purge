<?php

namespace Drupal\govcms_akamai_purge;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Implementation of a service to purge paths on Akamai.
 */
class PathPurger {

  public const PURGE_PATH_ENDPOINT = '/purge/path';

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The current request stack.
   * @param \GuzzleHttp\Client $client
   *   The HTTP client to make a request.
   * @param \Psr\Log\LoggerInterface $logger
   *   The log channel.
   */
  public function __construct(protected RequestStack $requestStack, protected Client $client, protected LoggerInterface $logger) {}

  /**
   * Check if all required env variables are set up correctly.
   *
   * @return bool
   *   FALSE if an env var is not set.
   */
  public function checkEnvironmentSetup() : bool {
    $purge_service_hostname = getenv('AKAMAI_PURGE_SERVICE_HOSTNAME');
    $purge_service_port = getenv('AKAMAI_PURGE_SERVICE_PORT');
    $purge_service_scheme = getenv('AKAMAI_PURGE_SERVICE_SCHEME');

    return !(
      empty($purge_service_hostname) ||
      empty($purge_service_port) ||
      empty($purge_service_scheme)
    );
  }

  /**
   * Retrieve the purge host.
   *
   * Attempt to look up environment variables for the configured akamai purge
   * hostname this is used to send a FQDN+path request to the purge intermediary
   * service.
   *
   * @return string
   *   Scheme and hostname for the project.
   */
  public function getHttpHost() : string {
    $registered_lagoon_routes = explode(',', getenv('LAGOON_ROUTES'));
    $akamai_purge_host = getenv('AKAMAI_PURGE_HOST');

    // Allow environment variable override for the purge host so this can
    // be controlled by operators. Akamai will reject requests if the
    // domain is not known, so this will allow definitions of the
    // purge domain when it cannot be correctly inferred.
    if (!empty($akamai_purge_host)) {
      return $akamai_purge_host;
    }

    foreach ($registered_lagoon_routes as $route) {
      if (str_contains($route, 'amazee.io')) {
        continue;
      }

      if (str_contains($route, 'govcms.gov.au')) {
        continue;
      }

      $akamai_purge_host = $route;
    }

    return empty($akamai_purge_host) ? $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost() : $akamai_purge_host;
  }

  /**
   * Validate a list of paths.
   *
   * @param string[] $paths
   *   List of paths to validate.
   *   The paths will be trimmed and empty paths will be removed.
   *
   * @return bool|array
   *   TRUE if all paths are valid, otherwise invalid paths are returned.
   */
  public function validatePaths(array &$paths): bool|array {
    $paths = array_filter(array_map(
      static fn ($value) => is_null($value) ? NULL : trim($value),
      $paths
    ));
    if (empty($paths)) {
      return FALSE;
    }

    $invalid_paths = [];
    $basename = $this->getHttpHost();

    foreach ($paths as &$path) {
      if (filter_var($path, FILTER_VALIDATE_URL)) {
        // Only accept paths - this will validate if full URLs are provided.
        $invalid_paths[] = $path;
        continue;
      }
      $path = '/' . ltrim($path, '/');
      $path = filter_var($path, FILTER_SANITIZE_URL);

      if (!filter_var($basename . $path, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
        $invalid_paths[] = $path;
      }
    }
    unset($path);

    if (count($invalid_paths) > 0) {
      return $invalid_paths;
    }

    return TRUE;
  }

  /**
   * Purge multiple paths.
   *
   * @param string[] $paths
   *   Paths to purge. The paths should be validated beforehand.
   *
   * @throws \GuzzleHttp\Exception\ClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Exception
   */
  public function purge(array $paths) : void {
    $purge_service_hostname = getenv('AKAMAI_PURGE_SERVICE_HOSTNAME');
    $purge_service_port = getenv('AKAMAI_PURGE_SERVICE_PORT');

    $opt = [
      'verify' => FALSE,
      'headers' => [
        'X-Purge-Token' => getenv('AKAMAI_PURGE_TOKEN'),
        'X-Lagoon-Project' => getenv('LAGOON_PROJECT'),
        'Referer' => $this->getHttpHost(),
      ],
      'json' => ['paths' => $paths],
    ];

    try {
      $this->client->request(
        'POST',
        "$purge_service_hostname:$purge_service_port" . static::PURGE_PATH_ENDPOINT,
        $opt
      );
    }
    catch (ClientException $exception) {
      // Log the client error and rethrow the exception.
      $response = Json::decode($exception->getResponse()?->getBody());
      $this->logger->error('client exception: ' . $response['reason'] ?? 'unknown');
      throw $exception;
    }
    catch (GuzzleException | \Exception $exception) {
      $this->logger->error('path purge error: ' . $exception->getMessage());
      throw $exception;
    }
  }

}
