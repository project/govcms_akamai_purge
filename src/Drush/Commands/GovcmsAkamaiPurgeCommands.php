<?php

namespace Drupal\govcms_akamai_purge\Drush\Commands;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\govcms_akamai_purge\PathPurger;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implementation of Drush support for GovCMS Akamai Purge.
 */
final class GovcmsAkamaiPurgeCommands extends DrushCommands implements ContainerInjectionInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\govcms_akamai_purge\PathPurger $pathPurger
   *   Path Purger service.
   */
  public function __construct(protected readonly PathPurger $pathPurger) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) : self {
    return new self(
      $container->get('govcms_akamai_purge.path_purger')
    );
  }

  /**
   * Command to purge paths.
   *
   * @param string[] $paths
   *   Paths to purge.
   */
  #[CLI\Command(name: 'govcms-akamai-purge:purge-paths', aliases: ['gap:purge-paths', 'gap:pp', 'gap-pp'])]
  #[CLI\Argument(name: 'paths', description: 'Paths to purge.')]
  #[CLI\Usage(name: 'govcms-akamai-purge:purge-paths /foo/bar', description: 'Purge the path /foo/bar')]
  #[CLI\Usage(name: 'govcms-akamai-purge:purge-paths /foo /bar /foo/bar', description: 'Purge the paths /foo, /bar, and /foo/bar')]
  public function purgePath(array $paths) : void {
    if (empty($paths)) {
      $this->logger()->error(dt('No path is provided.'));
      return;
    }

    if (!$this->pathPurger->checkEnvironmentSetup()) {
      $this->logger()->error(dt('GovCMS Akamai purge is not configured correctly, please contact support to restore functionality.'));
      return;
    }

    $invalid_paths = $this->pathPurger->validatePaths($paths);
    if ($invalid_paths === FALSE) {
      $this->logger()->error(dt('Please provide valid paths.'));
      return;
    }
    if (is_array($invalid_paths) && count($invalid_paths) > 0) {
      $this->logger()->error(dt('The paths that you have submitted are not in the correct format: %invalid_paths.', [
        '%invalid_paths' => implode(', ', $invalid_paths),
      ]));
      return;
    }

    if (count($paths) > 200) {
      $this->logger()->error(dt('Unable to process more than 200 paths in a single request.'));
      return;
    }

    try {
      $this->pathPurger->purge($paths);
    }
    catch (ClientException | GuzzleException | \Exception $e) {
      $this->logger()->error('Unable to purge paths at this time, please contact support.');
      return;
    }

    $this->logger()->success(dt('Your request has successfully been sent for processing. Please allow 10 minutes for the request to be actioned.'));
  }

}
